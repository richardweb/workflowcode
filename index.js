const express = require("express");
const app = express();
const port = 3000;

app.get("/", (req, res) => {
  res.send(
    "Hello World! My name is Richard Restrepo López, Email: richard.restrepo10@gmail.com,  profession: Systems Engineer"
  );
});

app.listen(port, () => {
  console.log(`Aplication runnning on Port: ${port}`);
});
